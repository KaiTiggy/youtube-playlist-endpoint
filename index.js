const express = require('express')
const youtube = require('youtube-api')
const readJson = require('r-json')
const cors = require('cors')
const app = express()
const port = 3000

app.use(cors())

const credentials = readJson(`${__dirname}/credentials.json`)
var tokens = {}

const oauth = youtube.authenticate({
  type: 'oauth',
  client_id: credentials.clientId,
  client_secret: credentials.clientSecret,
  redirect_url: credentials.redirect
})

app.get('/auth', (req, res) => {
  const oauthLink = oauth.generateAuthUrl({
    access_type: 'offline',
    scope: ['https://www.googleapis.com/auth/youtube.force-ssl']
  })

  const displayLink = `
    <a href="${oauthLink}">Click here to authorize with YouTube</a>
  `

  res.send(displayLink)
})

app.get('/auth/callback', (req, res) => {
  oauth.getToken(req.query.code, (err, newTokens) => {
    if (err) {
      res.send('Something went wrong')
      console.error(err)
      return false
    }
    tokens = newTokens
    res.send('Tokens saved')
    console.log({ tokens })
  })
})

app.get('/playlist/:playlistId/add/:videoId', (req, res) => {
  const { playlistId, videoId } = req.params

  oauth.setCredentials(tokens)

  const listRequest = {
    part: 'snippet,contentDetails',
    maxResults: 25,
    playlistId
  }

  youtube.playlistItems.list(listRequest, (err, data) => {
    if (err) {
      res.send('Something went wrong')
      console.error(err)
      return false
    }

    const videos = data.data.items.map(video => (video.contentDetails.videoId))
    console.log(videos)

    if (videos.includes(videoId)) {
      res.send('video already in playlist')
      return false
    }

    const insertRequest = {
      resource: {
        snippet: {
          playlistId,
          position: 0,
          resourceId: {
            kind: 'youtube#video',
            videoId
          }
        }
      },
      part: 'snippet'
    }

    youtube.playlistItems.insert(insertRequest, (err, data) => {
      if (err) {
        res.send('Something went wrong')
        console.error(err)
        return false
      }
      res.send('Added video successfully')
    })
  })
})

app.listen(port, () => {
  console.log(`YouTube Playlist Endpoint listening at http://localhost:${port}`)
})
